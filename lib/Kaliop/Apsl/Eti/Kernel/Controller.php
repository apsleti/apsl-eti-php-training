<?php

namespace Kaliop\Apsl\Eti\Kernel;


use Kaliop\Apsl\Eti\Controller\Action;
use Kaliop\Apsl\Eti\Controller\Error404Action;
use Kaliop\Apsl\Eti\DB\Connection;
use Kaliop\Apsl\Eti\DB\Manager;
use Kaliop\Apsl\Eti\Model\Tournament;
use Kaliop\Apsl\Eti\Model\TournamentMapping;
use Kaliop\Apsl\Eti\Request\Request;
use Kaliop\Apsl\Eti\Response\Response;


class Controller
{
    const DEFAULT_ACTION = 'index';
    const ERROR_404_ACTION = 'Error404';
    const ACTION_CLASS_PREFIX = "Kaliop\\Apsl\\Eti\\Controller\\";
    const ACTION_CLASS_SUFFIX = 'Action';
    const DATABASE_NAME = 'apsl_eti';
    const DATABASE_USER = 'apsleti';
    const DATABASE_PASSWORD = 'apsleti';

    /**
     * @var Manager
     */
    private $dbManager;

    public function run() {
        $this->buildDatabaseMappings();
        $request = new Request();
        $response = $this->handle($request);

        $response->send();
    }

    /**
     * @param Request $request
     * @return Response
     */
    protected function handle(Request $request) {
        $action = $this->createAction($request);
        $response = $action->execute();

        return $response;
    }

    /**
     * @param Request $request
     * @return Action
     * @throws \Exception
     */
    protected function createAction(Request $request) {
        $actionName = self::ACTION_CLASS_PREFIX . ucfirst($request->get('action', self::DEFAULT_ACTION)) . self::ACTION_CLASS_SUFFIX;

        $action = (class_exists($actionName)) ? new $actionName() : new Error404Action();
        if ($action instanceof Action) {
            $action->setRequest($request);
            $action->setDbManager($this->getDbManager());

            return $action;
        }

        throw new \Exception("Class '{$actionName}' is not an action.");
    }

    public static function getViewDir()
    {
        return __DIR__ . '/../../../../views';
    }

    /**
     * @return Manager
     */
    public function getDbManager()
    {
        if (!$this->dbManager instanceof Manager) {
            $this->dbManager = new Manager(new Connection(self::DATABASE_NAME, self::DATABASE_USER, self::DATABASE_PASSWORD));
        }
        return $this->dbManager;
    }

    protected function buildDatabaseMappings()
    {
        $this->getDbManager()->setMapping(Tournament::class, new TournamentMapping());
    }
}
