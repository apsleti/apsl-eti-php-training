<?php

namespace Kaliop\Apsl\Eti\Controller;

use Kaliop\Apsl\Eti\Form\Form;
use Kaliop\Apsl\Eti\Form\InputField;
use Kaliop\Apsl\Eti\Form\SelectField;
use Kaliop\Apsl\Eti\Form\SubmitField;
use Kaliop\Apsl\Eti\Model\Tournament;
use Kaliop\Apsl\Eti\Request\Request;
use Kaliop\Apsl\Eti\Response\RedirectResponse;
use Kaliop\Apsl\Eti\Validator\StringLengthValidator;

class AddTournamentAction extends Action
{
    protected function doExecute()
    {
        $form = new Form('index.php?action=addTournament', 'tournament');

        $nameField = new InputField('name', 'Unnamed team', 'Nazwa drużyny');
        $nameField->setValidators([
            new StringLengthValidator(8, 32)
        ]);

        $form->addField($nameField);
        $form->addField(new InputField('teamCount', '', 'Liczba drużyn', 'number'));
        $form->addField(new SelectField('type', Tournament::getAllowedTypes(), 'Typ drużyny'));
        $form->addField(new SubmitField('addTournamentAction', 'Dodaj turniej'));

        // TODO: Dodanie validatorów NotNull, bo żadna wartość nie może być pusta

        if ($this->request->isMethod(Request::METHOD_POST)) {
            $form->bind($this->request);

            $errors = $form->validate();

            if (empty($errors)) {
                // INFO: utworzenie obiektu tournament
                $tournament = new Tournament();
                $tournament->setName($form->getValue('name'));
                $tournament->setType($form->getValue('type'));
                $tournament->setTeamCount($form->getValue('teamCount'));

                // Zapisanie w bazie danych
                $this->dbManager->save($tournament);

                return new RedirectResponse('index.php');
            }

            $this->view->addParameter('errors', $errors);
        }

        $this->view->addParameter('form', $form);
    }
}
