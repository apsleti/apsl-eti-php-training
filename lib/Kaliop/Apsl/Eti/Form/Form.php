<?php

namespace Kaliop\Apsl\Eti\Form;
use Kaliop\Apsl\Eti\Request\Request;

class Form
{
    protected $values;

    /**
     * @var string
     */
    protected $action;

    /**
     * @var string
     */
    protected $method;

    /**
     * @var AbstractField[]
     */
    protected $fields;

    /**
     * @var string
     */
    protected $name;

    /**
     * Form constructor.
     * @param $action
     * @param string $method
     */
    public function __construct($action, $name, $method = Request::METHOD_POST)
    {
        $this->action = $action;
        $this->method = $method;
        $this->name = $name;
        $this->fields = [];
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @param array $fields
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
    }

    public function addField($field)
    {
        $this->fields[] = $field;
    }

    public function render()
    {
        $str = sprintf('<form action="%s" method="%s">', $this->action, $this->method);

        foreach ($this->fields as $field) {
            $str .= sprintf('<div>%s</div>', $field->render($this->name));
        }

        $str .= '</form>';

        return $str;
    }

    /**
     * @return array|null
     * @throws \Exception
     */
    public function validate()
    {
        if (!$this->values) {
            throw new \Exception('Form must be bound before validation');
        }

        $formErrors = [];

        foreach ($this->fields as $field) {
            $fieldErrors = [];
            $validators = $field->getValidators();
            foreach ($validators as $validator) {
                $value = isset($this->values[$field->getName()]) ? $this->values[$field->getName()] : '';
                $errors = $validator->validate($value);
                if ($errors !== true) {
                    $fieldErrors += $errors;
                }
            }

            if (!empty($fieldErrors)) {
                $formErrors[$field->getLabel()] = $fieldErrors;
            }
        }

        return empty($formErrors) ? null : $formErrors;
    }

    /**
     * @param $name
     * @return null
     * @throws \Exception
     */
    public function getValue($name)
    {
        if (!$this->values) {
            throw new \Exception('Form is not bound');
        }

        return isset($this->values[$name]) ? $this->values[$name] : null;
    }

    public function bind(Request $request) {
        if ($request->isMethod($this->method) && $request->has($this->name)) {
            $this->values = $request->get($this->name);
        }
    }
}
