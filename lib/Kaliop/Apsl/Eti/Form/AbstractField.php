<?php
/**
 * Created by PhpStorm.
 * User: grzes
 * Date: 19.05.16
 * Time: 12:53
 */

namespace Kaliop\Apsl\Eti\Form;


use Kaliop\Apsl\Eti\Validator\AbstractValidator;

abstract class AbstractField
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var mixed
     */
    protected $value;

    /**
     * @var string
     */
    protected $label;

    /**
     * @var AbstractValidator[]
     */
    protected $validators = [];
    
    // TODO: add id for fields
    // TODO: add form relation to build field name/id with form name/id

    /**
     * AbstractField constructor
     * 
     * @param $name
     * @param string $value
     * @param string $label
     */
    public function __construct($name, $value = '', $label = '')
    {
        $this->name = $name;
        $this->value = $value;
        $this->label = $label;
    }

    /**
     * Renders field
     *
     * @param string $formName
     * @return string
     */
    abstract public function render($formName = '');

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function generateName($formName = '')
    {
        return empty($formName) ? $this->name : sprintf('%s[%s]', $formName, $this->name);
    }

    /**
     * @return \Kaliop\Apsl\Eti\Validator\AbstractValidator[]
     */
    public function getValidators()
    {
        return $this->validators;
    }

    /**
     * @param \Kaliop\Apsl\Eti\Validator\AbstractValidator[] $validators
     */
    public function setValidators($validators)
    {
        $this->validators = $validators;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }
}
