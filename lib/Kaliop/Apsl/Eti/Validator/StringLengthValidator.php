<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 25.05.16
 * Time: 11:43
 */

namespace Kaliop\Apsl\Eti\Validator;


class StringLengthValidator extends AbstractValidator
{
    protected $min;

    protected $max;

    public function __construct($min = false, $max = false)
    {
        $this->min = $min;
        $this->max = $max;
    }

    public function validate($value)
    {
        $strLen = mb_strlen($value);
        $errors = [];

        if ($this->max !== false && $strLen > $this->max) {
            $errors[] = sprintf('Podana wartość jest dłuższa niż %d znaków', $this->max);
        }

        if ($this->min !== false && $strLen < $this->min) {
            $errors[] = sprintf('Podana wartość jest krótsza niż %d znaków', $this->min);
        }

        return empty($errors) ? true : $errors;
    }
}