<?php

namespace Kaliop\Apsl\Eti\Request;


class Request
{
    const METHOD_POST = 'POST';
    const METHOD_GET = 'GET';

    /**
     * @param string $method
     *
     * @return bool
     */
    public function isMethod($method) {
        return ($_SERVER['REQUEST_METHOD'] == strtoupper($method));
    }

    /**
     * @param $param
     *
     * @return mixed
     */
    public function get($param, $default = null) {
        return isset($_REQUEST[$param]) ? $_REQUEST[$param] : $default;
    }

    /**
     * @param $param
     *
     * @return bool
     */
    public function has($param) {
        return isset($_REQUEST[$param]);
    }
}
