<?php

namespace Kaliop\Apsl\Eti\View;

use Kaliop\Apsl\Eti\Kernel\Controller;

class View
{
    const DEFAULT_LAYOUT = 'layout.html.php';

    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $parameters;

    /**
     * @var string
     */
    protected $layout;

    /**
     * View constructor
     *
     * @param $name
     * @param array $parameters
     * @param string $layout
     */
    public function __construct($name, $parameters = [], $layout = self::DEFAULT_LAYOUT)
    {
        $this->name = $name;
        $this->parameters = $parameters;
        $this->layout = $layout;
    }

    /**
     * @throws \Exception
     */
    public function render()
    {
        if (empty($this->name)) {
            throw new \Exception('View name cannot be empty');
        }

        $templateFileName = Controller::getViewDir() . '/' . $this->name . '.html.php';

        if (!file_exists($templateFileName)) {
            throw new \Exception(sprintf('Template file %s does not exist', $templateFileName));
        }

        // In templates we will have access to parameters by $viewParams
        $viewParams = $this->parameters;

        // We start to buffer the output (no output will be sent by script)
        ob_start();
        include $templateFileName;

        // We are getting buffer content and save it in a variable
        $content = ob_get_contents();

        // We end buffering the output and clear it
        ob_end_clean();

        // In the layout we can echo all contents from $templateFileName with $content variable
        $layoutFileName = Controller::getViewDir() . '/' . $this->layout;
        if (file_exists($layoutFileName)) {
            include $layoutFileName;
        }
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param array $parameters
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
    }

    public function addParameter($name, $value)
    {
        $this->parameters[$name] = $value;
    }

    /**
     * @return string
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * @param string $layout
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;
    }
}
