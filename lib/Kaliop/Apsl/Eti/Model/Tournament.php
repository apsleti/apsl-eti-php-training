<?php

namespace Kaliop\Apsl\Eti\Model;

use Kaliop\Apsl\Eti\DB\EntityInterface;
use Kaliop\Apsl\Eti\DB\MappingInterface;

class Tournament implements EntityInterface
{
   /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var integer
     */
    protected $type;

    /**
     * @var string
     */
    protected $teamCount;

    /**
     * @return array
     */
    public static function getAllowedTypes()
    {
        return array(
            1 => 'Standardowy',
            2 => 'Podwójnej eliminacji'
        );
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTeamCount()
    {
        return $this->teamCount;
    }

    /**
     * @param mixed $teamCount
     */
    public function setTeamCount($teamCount)
    {
        $this->teamCount = $teamCount;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function getTypeName()
    {
        $types = $this->getAllowedTypes();

    }
}
