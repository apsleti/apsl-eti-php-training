<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 25.05.16
 * Time: 12:27
 */

namespace Kaliop\Apsl\Eti\DB;


interface EntityInterface
{
    // INFO: przeniesienie poprzednich metod do MappingInterface, gdyż definicja jest per klasa nie obiekt
    // Encja ma mapowanie do bazy, więc pobierzmy tylko Mapping

    /**
     * @return mixed
     */
    public function getId();
}
