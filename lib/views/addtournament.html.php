<h2>Dodaj turniej</h2>

<?php if (isset($viewParams['errors'])): ?>
    <ul class="alert alert-danger">
        <?php foreach ($viewParams['errors'] as $label => $fieldErrors): ?>
            <li>
                <?php echo $label ?>
                <ul>
                    <?php foreach ($fieldErrors as $error): ?>
                        <li><?php echo $error ?></li>
                    <?php endforeach ?>
                </ul>
            </li>
        <?php endforeach ?>
    </ul>
<?php endif ?>

<?php echo $viewParams['form']->render() ?>
