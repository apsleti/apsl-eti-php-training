<?php

require_once __DIR__ . '/lib/Kaliop/Apsl/Eti/Util/Autoloader.php';


$autoloader = new \Kaliop\Apsl\Eti\Util\Autoloader('lib', '.php');


spl_autoload_register([$autoloader, 'load']);
