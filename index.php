<?php

// TODO: Determine our environment by IP or system env
$env = 'dev';

require_once 'autoload.php';

use \Kaliop\Apsl\Eti\Kernel\Controller;

try {
    $controller = new Controller();
    $controller->run();
}
catch (\Exception $exception) {
    if ($env === 'dev') {
        throw $exception;
    } else {
        die("Trwa konserwacja systemu. Zapraszamy ponownie za kilka minut.");
        // TODO: log exception message and notify system admin
    }
}
